import { Module } from '@nestjs/common';
import { DNAModule } from './modules/dna/dna.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { config, databaseConfig } from './config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configValidationSchema } from './config/validation.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config, databaseConfig],
      isGlobal: true,
      validationSchema: configValidationSchema
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    DNAModule
  ],
})
export class AppModule {}
