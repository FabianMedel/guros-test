import { registerAs } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as dotenv from 'dotenv';
import { Mutation } from 'src/modules/dna/entities/mutation.entity';

dotenv.config(); 
const { env } = process;

export const databaseConfig = registerAs('database', () => ({
    type: 'mongodb',
    url: env.DB_HOST,
    entities: [
        Mutation
    ],
    useUnifiedTopology: true
}));