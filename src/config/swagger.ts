import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
    .setTitle('DNA mutation')
    .setVersion('1.0')
    .build();