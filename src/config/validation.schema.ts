import * as Joi from '@hapi/joi';

export const configValidationSchema = Joi.object({
    DB_HOST: Joi.string().default('mongodb://mongodb/dna'),
    PORT: Joi.number().default(3000).required()
});