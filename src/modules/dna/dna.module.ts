import { Module } from '@nestjs/common';
import { DNAService } from './dna.service';
import { DNAController } from './dna.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MutationRepository } from './entities/mutation.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MutationRepository
    ]),
  ],
  controllers: [DNAController],
  providers: [DNAService]
})
export class DNAModule {}
