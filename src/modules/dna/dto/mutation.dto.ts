import { ApiProperty } from '@nestjs/swagger';

export class MutationDto {
    @ApiProperty({
        name: "dna",
        example: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
        type: Array
    })
    dna: string []; 
}
