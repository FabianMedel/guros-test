import { HttpException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { DNAService } from './dna.service';
import { MutationRepository } from './entities/mutation.repository';

const mockMutationRepository = () => ({
  mutationStats: jest.fn(),
  createMutation: jest.fn()
});

const mockMutationTrue = {
  code: 200
};

const mockMutationFalse = {
  statusCode: 403,
  message: "No mutation"
};

describe('MutationService', () => {
  let dnaService: DNAService;
  let mutationRepository;  

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DNAService,
        { provide: MutationRepository, useFactory: mockMutationRepository },
      ],
    }).compile();

    dnaService = module.get(DNAService);
    mutationRepository = module.get(MutationRepository);
  });

  describe('existMutation', () => {
    it('calls hasMutation and return 403', async () => {
      const result = await dnaService.existsMutation({dna:["ATGCGA", "CAGTGC", "TTATTT", "AGAAGG", "GCGTCA", "TCACTG"] });
      expect(result).toEqual({
        code: 403,
        error_message: "No mutation"
      });
    });
  });

  describe('existMutation', () => {
    it('calls hasMutation and return 200', async () => {
      const result = await dnaService.existsMutation({ dna:["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] });
      expect(result).toEqual({
        code: 200
      });
    });
  });

  describe('existMutation', () => {
    it('calls hasMutation and return 403 "letra invalida"', async () => {
      const result = await dnaService.existsMutation({ dna:["ATGCGZ", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] });
      expect(result).toEqual({
        code: 403,
        error_message: "Hay un caracter invalido, solo se permiten los siguientes caracteres: A, T, C, G"
      });
    });
  });
  
});
