import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { MutationDto } from './dto/mutation.dto';
import { DNA } from './../mutation';
import { InjectRepository } from '@nestjs/typeorm';
import { Mutation } from './entities/mutation.entity';
import { MutationRepository } from './entities/mutation.repository';


@Injectable()
export class DNAService {
  constructor (
    @InjectRepository(MutationRepository) private mutationRepository: MutationRepository
  ) {}

  async existsMutation({ dna }: MutationDto) {
    try {
      const isMutation = DNA.hasMutation(dna)

      await this.mutationRepository.createMutation(dna, isMutation);
      
      if(!isMutation) {
        throw new Error('No mutation');
      }

      return {
        code: HttpStatus.OK
      }
    } catch (error) {
      return {
        code: HttpStatus.FORBIDDEN,
        error_message: error.message
      }
      //throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  async mutationStats() {
    try {
      return  await this.mutationRepository.mutationStats();
    } catch (error) {
      throw new HttpException('No stats',HttpStatus.FORBIDDEN);
    }
  }
}
