import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { DNAService } from './dna.service';
import { MutationDto } from './dto/mutation.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Mutation')
@Controller('mutation')
export class DNAController {
  constructor(private readonly dnaService: DNAService) {}

  @Post()
  existsMutation(@Body() MutationDto: MutationDto) {
    return this.dnaService.existsMutation(MutationDto);
  }

  @Get('/stats')
  mutationStats() {
    return this.dnaService.mutationStats();
  }
}
