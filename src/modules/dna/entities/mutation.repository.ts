import { InternalServerErrorException } from "@nestjs/common";
import { EntityRepository } from "typeorm";
import { Repository } from "typeorm/repository/Repository";
import { Mutation} from "./mutation.entity";


@EntityRepository(Mutation)
export class MutationRepository extends Repository<Mutation> {

    async createMutation(dna: string[], exists_mutation: boolean){
        try {
            const mutation = this.create({
                dna,
                exists_mutation
            });
          
            await this.save(mutation)
        } catch (error) {
            console.log(error)
            throw new InternalServerErrorException();
        }
    }

    async mutationStats(){
        try {
            const count_mutations = await this.count({exists_mutation: true});
            const count_no_mutation = await this.count({exists_mutation: false});

            return {
                count_mutations,
                count_no_mutation,
                ratio: count_no_mutation/count_mutations
            }
        } catch (error) {
            console.log(error)
            throw new InternalServerErrorException();
        }
    }

}
