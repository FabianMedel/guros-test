import { Column, CreateDateColumn, Entity, ObjectIdColumn } from "typeorm";
import { ObjectID } from "typeorm/driver/mongodb/typings";

@Entity()
export class Mutation {
    @ObjectIdColumn()
    id: ObjectID;

    @Column({ nullable: false, unique: true })
    dna: string[];

    @Column()
    exists_mutation: boolean; 

    @CreateDateColumn({ type: 'timestamp'})
    createAt: Date;
}
