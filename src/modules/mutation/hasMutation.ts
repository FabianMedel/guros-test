import { isHorizontal } from "./isHorizontal";
import { isOblique } from "./isOblique";
import { isObliqueInvert } from "./isObliqueInvert";
import { isValid } from "./isValid";
import { isVertical } from "./isVertical";
import { existMutation, resetData } from "./mutation";

export function hasMutation(dna): boolean {
    const rows = dna.length
    const cols = dna[0].length
    let mutationsCount = 0
  
    if ((rows >= 4) || (cols >= 4)) {
      dna.forEach((element, row) => {
        for(let col = 0; col < element.length; col++) {
          if (isValid(dna[row].charAt(col))) {
            if (!existMutation([col, row])) {
              if (isHorizontal(col, row, dna) || isVertical(col, row, dna) || isOblique(col, row, dna) || isObliqueInvert(col, row, dna)) {
                mutationsCount++
              }
            }
          } else {
            throw new Error('Hay un caracter invalido, solo se permiten los siguientes caracteres: A, T, C, G')
          }
        }
      });
    }
  
    resetData()
  
    if (mutationsCount >= 2) {
      return true
    }
  
    return false
  }